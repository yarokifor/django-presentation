# Django Presentation

Yaro Kifor

What is django?
Django is a python based web framework that allows for rapid devoplement. 
It provides many tools for making a modern day webapp. People who that use 
Django: Instgram, Lockheed Martin and Mozilla.

# Index
  * Http Protocol
  * Model View Controller
  * Django project overview
  * Models
  * Views
  * Url paths

## HTTP Protocol

##### Request
Bare minimum

`GET /`

More typical

```
GET / HTTP/1.1
Host: slashdot.com
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Upgrade-Insecure-Requests: 1
```

It all start with the method of the request. There are many method based off of RFC's standards. Some commons ones are: 
GET, POST, PUT, DELETE.

https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Summary_table


##### Response
```
HTTP/1.1 301 Moved Permanently
Server: nginx/1.13.9
Date: Tue, 19 Feb 2019 22:48:38 GMT
Content-Type: text/html
Content-Length: 185
Connection: keep-alive
Location: https://slashdot.org/

<html>
<head><title>301 Moved Permanently</title></head>
<body bgcolor="white">
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx/1.13.9</center>
</body>
</html>
```

As you can see there are some headers and a body. The body is what you normal see, but render by your browser. 
The header tell the client about the size and type of body and some other information. This also where cookies can be 
put into browser. Your browser will put the cookies in the header of any request to that site.

## Model View Controller Paradigm

The user uses the controller to manipulate the model. The user uses views to see 
model. Django a view is not a the as a view in the paradigm. The model is 
typical some sort of database.

### Paradigm
```text
    +-----------+                 +------------+
    |           |                 |            |
    |   Model   |  <------------  | Controller |
    |           |                 |            |
    +-----------+                 +------------+
          |                             ^ 
          |                             |
          |                             |
          v                             |
    +-----------+                 +------------+
    |           |                 |            |
    |   View    |  ------------>  |    User    |
    |           |                 |            |
    +-----------+                 +------------+
```

### Reality 
```text
    +-----------+                 +------------+
    |           |                 |            |
    |   Model   |  <------------  | Controller |
    |           |                 |            |
    +-----------+                 +------------+
       ^                                ^
       |      _________________________/
       |     /
       v    v
    +-----------+                 +------------+
    |           |                 |            |
    |   View    |  <----------->  |    User    |
    |           |                 |            |
    +-----------+                 +------------+
```

Django views are the interface between the user and web server. Most of the web 
logic is in a Djagno view. The view interacts with Django's ORM (Object Relational Map) to
view the model itself and to manipulate it. The controller is an external part of the project.
Normally a library of some sort, but a controller isn't required.

## Django project overview
Heir of standard Django project
```
src
├── manage.py
├── my_site
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── photo_feed
    ├── admin.py
    ├── apps.py
    ├── __init__.py
    ├── migrations
    │   └── __init__.py
    ├── models.py
    ├── tests.py
    └── views.py
```

Django divides a project in to apps. A project can contain one or more apps. In 
this case our project is called 'my_site' and our app is called 'photo_feed'.

#### Important files

##### Project files
`manage.py` used for bootstraping application and deployment. 
Chance are you'll never need to edited this file.

`setting.py` contains project settings like which database to use, timezone, and
other infomation.

`urls.py` contains a mapping of url paths to views.

##### App files
`admin.py` contains how the model will interact with Django's admin app.

`apps.py` contains app specific configurations.

`migration/` will contain all migration for your app.

`models.py` contains ORM infomation which is used for creating tables and
populating data.

`views.py` contains all the app logic.

`tests.py` contains tests for views



## Model

Django has a builtin ORM to handle storing of information into a databases. ORM stands for Object Relation Manager. 
It's job is to take a row of a table in a database and convert it to a python object. It can also update the row when a 
python object is updated.

Django ORM has support for Postgres, MySQL, Sqlite, and Oracle databases.

When you start a django project one of the first things your going to do is define a your own model. 
In this basic example we show a model that might be from a photo storage application.

##### models.py
```python
from django.db.models import Model, UUIDField, ImageField, \
                        TextField, ForeignKey, DateTimeField
from django.contrib.auth.models import User
from uuid import uuid4

class Photo(Model):
   id = UUIDField(primiary_key=True, default=uuid4)
   owner = ForeignKey(User, blank=False)
   file = ImageField(upload_to='photos/', blank=False)
   capitation = TextField(max_length=256, blank=False)
   creation = DateTimeField(auto_now_add=True)
```

As you can see we are inheriting the Model class. Every model corresponds to a 
table in a database. Every object should have an id where the field primiary_key
parameter is true.

To get a database to use this schema you must make and apply migration.

To make migrations do `manage.py makemigrations`. This will populate the migrations folder. A migration 
explains how to get a database's schema to a state where it can be used by the app.

To apply the migration do `manage.py migrate`. This will take the migration made from the command above and apply them 
to whatever the database is configure in the settings.py file. Now you models are ready to be used by your views.

Any time you make a change to your model you'll want to make another migrations and apply. This really handy for version
upgrading where your app's model change and you want to upgrade old instances of your app.

## View
Every Django view does takes in a HTTP Request and returns a HTTP Response. This is where most of the work of webapp 
comes in.

#### veiws.py
```python
from django.shortcuts import render, get_list_or_404
from .models import Photo

def index(request):
    return render(request, 'index.html')
    
def user_photos(request, username):
    photos = get_list_or_404(Photo, owner__username=username)
    return render(request, 'user_photos.html', context={'photos': photos})
```
Views can be very simple like index, where they just return a templated page. 
They can also be very complicated. Here is snippet from an employee time tracking app.

```python
@login_required 
def shifts(request):
    last_shift = Shift.objects.filter(user = request.user).last()
    last_event = Event.objects.filter(user = request.user).last()

    if request.method == 'POST':
        event = request.POST.get("event")
        newtime = request.POST.get("newtime")
        if event == "task" and last_shift != None:
            tasks = last_shift.tasks_completed
            task = bleach.clean(request.POST['desc']).replace('`','\'')
            if tasks == "":
                last_shift.tasks_completed = '`%s`' % task
            else:
                last_shift.tasks_completed = '%s,`%s`' % (tasks, task)
            last_shift.save()
        elif event == "update" and newtime != None:
            newtime_dt = datetime.datetime.strptime(newtime,'%Y-%m-%dT%H:%M:%S')
            other_events = Event.objects.filter(user = request.user, time__gt = newtime_dt)
            if len(other_events) > 1:
                messages.error(request, "Updated time cannot be earlier than any previous event.")
            elif newtime_dt > datetime.datetime.now():
                messages.error(request, "Updated time cannot be in the future.")
            else:
                last_event.time = newtime
                last_event.save()
                messages.info(request, "Last event time modified.")
        elif event in Event.REQUIRED_EVENT.keys():
            last_event = Event(time = timezone.now(), event = event, user = request.user)
            last_event.save()
            if event == "IN":
                last_shift = Shift(user = request.user, start = last_event, tasks_completed = "")
            elif event == "OUT":
                last_shift.end = last_event
            last_shift.save()
             
    context = {
        "last_shift": last_shift,
        "last_event": last_event,
        "lastest_events": Event.objects.filter(user = request.user),
        "event_choices": Event.EVENTS,
        "possible_events": Event.REQUIRED_EVENT[last_event.event if last_event != None else None]
    }
    if last_shift != None:
        context['tasks_completed'] = last_shift.tasks_completed[1:-1].split('`,`')

    return render(request, 'shifts.html', context)
```

While this isn't the best programming out there it shows how complicated a view can get. 

#### Templating

Templating is used to render content in HTML which can view by the user. This done with the render function.
Django template has support for inheriting other templates and overriding blocks. This allows you to avoid redoing many
tedious things in html and lets you focus more on programming.

##### base.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {% block head %}
    {% endblock %}
</head>
<body>
{% block body %}
{% endblock %}
</body>
</html>
```
##### user_photos.html
```html
{% extends 'base.html' %}

{% block head %}
{% endblock %}

{% block body %}
    {% load thumbnail %}
    {% for photo in photos %}
        <div>
            <a href="/photo/{{ photo.id }}">
                <img alt="{{ photo.id }}" style='float: left; padding: 2px' src="{{ photo.file|thumbnail_url }}">
            </a>
        </div>
    {% endfor %}
{% endblock %}
```

Django has a powerful built in templating feature, but also supports Jinja2.
## URL Paths
```python
urlpatterns = [
    url(r'^$', views.index, name='index')
    url(r'^upload$', views.new_upload_image, name='upload'),
    url(r'^photo/' + id_pattern + '$', views.photo, name='single_photo'),
    url(r'^user/' + username_pattern + '$', views.user_photos, name='user_photos'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```


## Other cool features and things

   * Caching
   * CSRF Protection
   * App Administration
   * Most things are modular
